extends Timer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	wait_time = 1
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	var grid = get_parent().find_node("AGrid")
	var res = grid.route()
	if res:
		if res.size() != 0:
			res.append(grid.pointB)
			for n in res:
				print (n.position)
				n.Body.color = n.targetColor
		stop()
		return res
	pass # Replace with function body.

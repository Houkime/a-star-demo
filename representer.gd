extends Node2D

class_name GridNode

var anglestep=6.28/30
var Body
var hovershade=0.5
var radius=10
var nodeColor=ColorN("cyan")
var hovercolor=ColorN("white")
var inactiveColor = ColorN("gray")
var textureLayer
var active = true
var targetColor = ColorN("orange")
var selectionCircle
var selectionRadius=32
var Selected=false
var target = false
var ignoreUnselection=false
var dragging=false
var locked=false
var history = Array() # history of nodes
var track_length = 0
var linkgroups=Array()

var _area = null
var _collider = null


export var nets = Array(["net1"])

func inactivate():
	active=false
	nodeColor=inactiveColor
	Body.color = inactiveColor

	

func _ready():
	selectionCircle=create_polygon(-2,selectionRadius,0,2*PI,ColorN("cyan"))
	add_child(selectionCircle)
	selectionCircle.visible=false
	Body=create_polygon(0,radius)
	add_child(Body)
	_area = Area2D.new()
	_collider = CollisionPolygon2D.new()
	_collider.polygon = Body.polygon
	_area.add_child(_collider)
	add_child(_area)
	selectionCircle
	textureLayer=TextureButton.new()
	add_child(textureLayer)
	textureLayer.connect("mouse_entered",self,"onMouseEnter")
	textureLayer.connect("mouse_exited",self,"onMouseExit")
	textureLayer.connect("button_down",self,"onButtonDown")
	textureLayer.connect("button_up",self,"onButtonUp")
	textureLayer.rect_position=Vector2(radius*-1,radius*-1)
	textureLayer.rect_size=Vector2(radius*2,radius*2)

func check_if_colliding():
	if _area.get_overlapping_areas().size() !=0:
		print("collision detected at", position)
		queue_free()
	else:
		#print("was unobscured at", position)
		_area.free()

func _process(delta):
	if dragging:
		position=get_global_mouse_position()
		for L in linkgroups:
			L.update_render()

func onButtonDown():
	Selected=true
	selectionCircle.visible=true
	ignoreUnselection=true
	var parent = get_parent()
	if not parent.pointA:
		print("selecting self as A")
		parent.pointA = self
		nodeColor = targetColor
		Body.color = targetColor
		target=true
	elif not parent.pointB:
		print("selecting self as B")
		parent.pointB = self
		nodeColor = targetColor
		Body.color = targetColor
		target=true
		parent.get_parent().find_node("Timer").start()
		
	#dragging=true

func onButtonUp():
	Selected=false
	selectionCircle.visible=false
	ignoreUnselection=false
	dragging=false

func onMouseEnter():
	Body.color=hovercolor

func onMouseExit():
	Body.color=nodeColor

func create_polygon(z,rad=100,startang=0,finishang=PI*2,col=ColorN("cyan",1)):
	
	if startang==finishang:
		return
	#centerpoint
	var varang=startang
	var pool=PoolVector2Array()
	pool.push_back(Vector2(0,0))
	#arch
	while (varang<=finishang):
		pool.push_back(rad*Vector2(cos(varang),sin(varang)))
		varang=varang+anglestep
	varang=finishang
	pool.push_back(rad*Vector2(cos(varang),sin(varang)))
	var Poly=Polygon2D.new()
	Poly.color=col
	Poly.polygon=pool
	var outline=Line2D.new()
	pool.remove(0)
	outline.points=pool
	outline.default_color=Poly.color.lightened(0.5)
	outline.width=1.5
	Poly.z_index=z
	outline.z_index=z
	Poly.add_child(outline)
	return Poly
	
#func _init(NodeName,nets):
#	pass

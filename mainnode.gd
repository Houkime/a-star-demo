extends Node2D


class_name AGrid


export var nx = 10
var ny = null
var step = 100
var margin = 50
var expansion_quota = 10

var Representer=load("res://representer.gd")
var Link=load("res://Link.gd")
var LinkGroup=load("res://LinkGroup.gd")
var nodes=Array()
var links=Array()
var nets=Array()
var linkgroups=Array()
var pointA = null
var pointB = null
var opti_route = null
var sth_added = false

var edgenodes = Array()
var edgenodes_next = Array()
var candidates = Array()

var phys_delay_timer =null

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func recheck_collisions():
	print("rechecking colliders")
	for n in nodes:
		n.check_if_colliding()
	

func recheck_node_validity():
	var deletion_queue = Array()
	for n in nodes:
		if not is_instance_valid(n):
				deletion_queue.append(n)
	for n in deletion_queue:
			nodes.remove(nodes.find(n))

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	phys_delay_timer = Timer.new()
	phys_delay_timer.wait_time = 0.2
	phys_delay_timer.one_shot = true
	add_child(phys_delay_timer)
	phys_delay_timer.start()
	phys_delay_timer.connect("timeout",self,"recheck_collisions")
	
	print("grid readying")
	step = (get_viewport_rect().size.x - margin)/nx
	ny = ( get_viewport_rect().size.y) / step
	ny = floor(ny) 
	for x in range(nx):
		for y in range(ny):
			var node = GridNode.new()
			node.position.x = x*step
			node.position.y = y*step
			add_child(node)
			if is_instance_valid(node):
				nodes.append(node)
	print("grid ready")

class Candidate:
	var node = null
	var previous = null
	var distance_to_target = null
	func _init(_previous,_node,target):
		node=_node
		previous=_previous
		distance_to_target = (node.position - previous.position).length() + (node.position-target.position).length()

func add_to_edge(candidate):
	if not candidate.node in edgenodes:
		edgenodes.append(candidate.node)
		candidate.node.history = candidate.previous.history.duplicate()
		candidate.node.history.append(candidate.previous)
		candidate.node.inactivate()
		sth_added = true
		if candidate.node == pointB:
			opti_route = candidate.node.history

func remove_from_edge(node):
	edgenodes.remove(edgenodes.find(node))
	node.history = null

func compare_candidates(a,b):
	if a.distance_to_target < b.distance_to_target: #if anything, reverse
		return true
	return false

func route():
	recheck_node_validity()
	print("starting routewave")
	edgenodes.append(pointA)
	pointA.inactivate()
	#var expansion_queue = Array()
	var marg = step*sqrt(2)*1.01

	for cur_node in edgenodes:
		var neighbours = Array()
		for n in nodes:
			if (cur_node.position - n.position).length() <= marg:
				neighbours.append(n)
		var has_active_neighbors = false
		for n in neighbours:
			if n.active:
				candidates.append(Candidate.new(cur_node,n,pointB))
				has_active_neighbors = true
		if not has_active_neighbors:
			edgenodes.remove(edgenodes.find(cur_node))
	if candidates.size()<=expansion_quota:
		for c in candidates:
			add_to_edge(c)
	else:
		candidates.sort_custom(self,"compare_candidates")
		for i in range(expansion_quota):
			add_to_edge(candidates[i])
	if opti_route:
		return opti_route
	if ! sth_added:
		return []
	sth_added = false
	return null

func find_route():
	while true:
		var res = route()
		if res:
			
			for n in res:
				print (n.position)
				n.Body.color = n.targetColor
			return res
			
#	print(nodes.size())
#	update_links()
#	for L in linkgroups:
#		add_child(L)
#
#
#func update_links():
#	var nodarr=Array()
#	for C in nodes:
#		for N in C.nets:
#			if !nets.has(N):
#				nets.push_front(N)
#	for N in nets:
#		for C in nodes:
#			if C.nets.has(N):
#				nodarr.push_front(C)
#		for i in range(nodarr.size()-1):
#			for k in range(i+1,nodarr.size()):
#				links.push_back(Link.new(nodarr[i],nodarr[k],N))
#		nodarr.clear()
#	var tlin=links.duplicate()
#	while tlin.size()!=0:
#		var linn=Array()
#		var tlin2=Array()
#		for L in tlin:
#			if (L.node1==tlin[0].node1)&&(L.node2==tlin[0].node2):
#				linn.append(L)
#			else:
#				tlin2.append(L)
#		linkgroups.append(LinkGroup.new(tlin[0].node1,tlin[0].node2,linn))
#		tlin=tlin2
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Button_button_down():
	get_tree().change_scene("res://Grid.tscn")
	pass # Replace with function body.

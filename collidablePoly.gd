extends Polygon2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _init():
	var area = Area2D.new()
	var collider = CollisionPolygon2D.new()
	collider.polygon = polygon
	area.add_child(collider)
	add_child(area)
	print ("collidable poly initialised")

func _ready():
	print("poly ready")
